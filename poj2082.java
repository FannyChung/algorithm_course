/**
 * 提交到OJ注意事项：改类名，去掉包名
 * http://blog.csdn.net/wuyanyi/article/details/7243580
 * http://blog.csdn.net/bobten2008/article/details/5116807
 */
package poj;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author ZhongFang
 *
 */
public class poj2082 {

	static int getMaxS(int h[],int w[]){
		int maxS = 0;
		Stack<Integer> stack=new Stack<Integer>();
		int i=0;
		int n=h.length;
		
		int tempW=0;
		while(i<n){
			if(!stack.isEmpty()&&h[stack.peek()]>h[i]){
				int top=stack.pop();
				tempW+=w[top];
				maxS=Math.max(maxS, h[top]*tempW);
			}else {
				stack.push(i);
				w[i]+=tempW;
				i++;
				tempW=0;
			}
		}
		
		tempW=0;
		while (!stack.isEmpty()) {
			int top=stack.pop();
			tempW+=w[top];
			maxS=Math.max(maxS, h[top]*tempW);
		}
		return maxS;
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		while (scanner.hasNext()) {
			int n=scanner.nextInt();
			if(n==-1)
				break;
			int h[]=new int[n];
			int w[]=new int[n];
			for (int i = 0; i < n; i++) {
				w[i]=scanner.nextInt();
				h[i]=scanner.nextInt();
			}
			System.out.println(poj2082.getMaxS(h, w));
		}
	}

}
